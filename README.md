# TabShow
TabShow is a chrome extension designed to create a slideshow of the open tabs in a window. The tabs will not rotate if that tab has any interaction with the mouse or keyboard.

## TODO
* change icon to simpler grey
* change icon to stop button when active?
* add options view with text input for number of seconds before transition