'use strict';
var next_tab;
var current_tab;
var active = false;
var cycle_time = 3000;
chrome.browserAction.onClicked.addListener(function (tab){
    chrome.tabs.query(
          {
              currentWindow: true
          }, 
          function (tabs){
            tabs.forEach(function(tab, i){
                if(active){
                    killCycleAlarm(tab.id);
                    current_tab = undefined;
                }else{
                    if(tab.active == true){
                        current_tab = tab;
                        addCycleAlarm(tab.id);
                    }
                }
                chrome.tabs.executeScript(tab.id, {file: "scripts/contentscript.js"});

            });
            
            if(active == true){
                chrome.alarms.onAlarm.removeListener(cycleTabs);
                active = false;
            }else{
                active = true;
                chrome.alarms.onAlarm.addListener(cycleTabs);
            }
          }
    );
    
});

chrome.runtime.onMessage.addListener(function(msg, _, sendResponse) {
    if (msg.resetAlarm) {
        killCycleAlarm(current_tab.id);
        addCycleAlarm(current_tab.id);
    } 
});

function killCycleAlarm(tab_id){
    chrome.tabs.sendMessage(tab_id, {kill: true});
    chrome.tabs.onUpdated.removeListener(onTabUpdated);
    chrome.alarms.clear("moveTab");
}
function addCycleAlarm(tab_id){
    chrome.tabs.sendMessage(tab_id, {start: true});
    chrome.alarms.create("moveTab", {when:  Date.now() + cycle_time});
    chrome.tabs.onUpdated.addListener(onTabUpdated);
}

function onTabUpdated(tab_id, change_info, tab){
    addCycleAlarm(tab_id);
}
function cycleTabs(){
    chrome.tabs.query(
        {
            currentWindow: true
        }, 
        function (tabs){
            console.log(tabs);
            var on_current = false;
            var i = 0;
            tabs.forEach(function(tab){
              if(current_tab.id == tab.id){
                  on_current = true;
                  var next_index = i == tabs.length-1 ? 0 : i+1;
                    if(next_tab = tabs[next_index]){
                        chrome.tabs.sendMessage(current_tab.id, {kill: true});
                        chrome.tabs.update(next_tab.id, {active: true, highlighted: true});
                        current_tab = next_tab
                        addCycleAlarm(current_tab.id);
                    }
                  return;
              }
              i++;
            });
        }
    );
}