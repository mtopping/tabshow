'use strict';

function resetTimer(event) {
    chrome.runtime.sendMessage({resetAlarm: true});
};

chrome.runtime.onMessage.addListener(function(msg, _, sendResponse) {
  if(msg.kill == true){
    window.removeEventListener('mousemove', resetTimer, true);
    window.removeEventListener('keydown', resetTimer, true);
  }else if(msg.start == true){
    window.addEventListener('mousemove', resetTimer, true);
    window.addEventListener('keydown', resetTimer, true);
  }
});